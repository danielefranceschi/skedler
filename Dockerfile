FROM skedler/reports:3.8.0

COPY head_logo.png /opt/skedler/client/public/img/head_logo.png

RUN mkdir -p /.backup/skedler/data \
            /.backup/skedler/config \
            /.backup/skedler/license \
    && cp -aR /opt/skedler/config/* /.backup/skedler/config/ \
    && apt-get update \
    && apt-get install -y python

#COPY /root/.local/share/data/bconf/ /.backup/skedler/license/
COPY ./reporting.yml /.backup/skedler/config/
COPY start.sh /

ENV KIBANA_USER="kibana" \
    KIBANA_PWD="changeme" \
    ELASTIC_USER="elastic" \
    ELASTIC_PWD="changeme" \
    GF_SECURITY_ADMIN_USER="admin" \
    GF_SECURITY_ADMIN_PASSWORD="changeme"

WORKDIR /opt/skedler

CMD ["/start.sh"]
